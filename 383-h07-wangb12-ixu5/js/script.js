function validate(){
	if($("#user").val().length < 3){
		$("#userError").show();
		return false;
	}
	else if($("#dropdownAffiliation").find(":selected").text() == ""){
		$("#userError").hide();
		$("#affError").show();
		return false;
	}	
	else if(
			($("#textarea").val().length < 3)
			|| ($("#textarea").val().indexOf(">") >= 0)
			|| ($("#textarea").val().indexOf("<") >= 0)
			|| ($("#textarea").val().lastIndexOf(" ") <= $("#textarea").val().indexOf(" "))
		 ) {
		$("#userError").hide();
		$("#affError").hide();
		$("#commentsError").show();
		return false;
	}
	else if($("#car").val() == ""){
		$("#userError").hide();
		$("#affError").hide();
		$("#commentsError").hide();
		$("#carError").show();
		return false;
	}
	else if($("#dropdownColor").find(":selected").text() == ""){
		$("#userError").hide();
		$("#affError").hide();
		$("#commentsError").hide();
		$("#carError").hide();
		$("#colorError").show();
		return false;
	}
		$("#userError").hide();
		$("#affError").hide();
		$("#commentsError").hide();
		$("#carError").hide();
		$("#colorError").hide();
	return true;
}

