var time = 0;

$(document).ready(function () {
    $("#content").text("Get busy living or get busy dying. -Stephen King");
    $("#clicks").text("the box has been clicked 0 times");
    $("#time").text(new Date().toString());
    $("#clicks").click(function () {
        time++;
        $("#clicks").text("the box has been clicked "+  time +" times");
    });
    setInterval(function () { $("#time").text(new Date().toString()); }, 5000);
});