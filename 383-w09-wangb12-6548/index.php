<!DOCTYPE html>

<?php
/*
YOUR COMMENT BLOCK HERE (name, date, assignment..)
*/
   session_start(); //create a session
   //initialize number of visits session variable
   $numVisits = 0;
   if (isset($_SESSION['num'])) {
      $_SESSION['num']++;
      $numVisits = $_SESSION['num'];
   } else {
      $numVisits = 0;
      $_SESSION['num'] = 0;

   }
//see if the cmd get variable is passed into the program.
   $cmd = "";
   if (isset($_GET['cmd'])) {
      $cmd =htmlspecialchars($_GET['cmd']);
      if ($cmd != "page1" && $cmd != "page2" && $cmd != "page3") {
         $cmd = "";
      }
   }
//YOU WILL PUT YOUR FORM HANDLING CODE HERE
   if(isset($_REQUEST['name'])){
    $_SESSION['user'] = $_REQUEST['name'];
   }
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Bo Wang Lab09</title>
    </head>

<body>

<nav class="navbar navbar-light bg-light sticky-top">
  <a class="navbar-brand">Homework09 - wangb12</a>
  </nav>
<div class="container ml-3">
    <div class="row">
      <div class="col-3">
        <ul class="nav flex-column">
          <li class="nav-item mt-3">
            <a href="index.php?cmd=page1">Random</a>
          </li>
          <li class="nav-item mt-3">
            <a href="index.php?cmd=page2">Images</a>
          </li>
          <li class="nav-item mt-3">
            <a href="index.php?cmd=page3">Input Form</a>
          </li>
          <li class="nav-item mt-3">
            <a href="index.php">Home</a>
          </li>
          <li class="list-group-item list-group-item-primary mt-3">
            <?php
              echo "Visits: ",$numVisits;
            ?>
          </li>
          <li class="list-group-item list-group-item-secondary mt-3">
            <?php
              if(isset($_SESSION['user'])){
                echo $_SESSION['user'];
              }
            ?>
          </li>
        </ul>
        </div>
      
      <div class="col-9">
        <?php
          if ($cmd=="page1"){
        ?>
        <h2>Random</h2>
        <table class="table">
            <?php
              for ($a = 0; $a < 20; $a++){
                echo '<tr>';
                for($b = 0; $b < 5; $b++){
                    echo '<td style="color:rgb(',mt_rand(0,255),',',mt_rand(0,255),',',mt_rand(0,255),')';
                    echo '">';
                    echo mt_rand(0,99999);
                    echo'</td>';
                  }
                echo '</tr>';
              }
            ?>
        </table>
        <?php
          } else if ($cmd == "page2") {
          	$posi = mt_rand(0,2);
          	$pic[0] = 'china.jpg';
          	$pic[1] = 'great.png';
          	$pic[2] = 'hello.png';
        	 echo '<h2>Pictures</h2>';
            echo '<img src= "images/',$pic[$posi],'"/>';
          } else if($cmd == "page3"){
        ?>
          <h2>Form</h2>
          <form method='post' action='index.php'>  
            <div class="form-group">
              <label for="name">Username</label>
              <input type='text' id= 'name' class="form-control" name='name'></input>   
            </div> 
            <input type='submit' class="btn btn-primary">             
          </form> 
        <?php
          } else {

        ?> 
        <h2>Home</h2>
          <table class="table">
          <thead class="thead-light">
            <tr>
              <th>Variable</th>
              <th>Value</th>
            </tr>
            </thread>
            <tbody>
            <tr>
              <th>HTTP_HOST</th>
              <td>
              <?php
                echo $_SERVER['HTTP_HOST'];
              ?>
              </td>
            </tr>
            <tr>
              <th>HTTP_USER_AGENT</th>
              <td>
              <?php
                echo $_SERVER['HTTP_USER_AGENT'];
              ?></td>
            </tr>
            <tr>
              <th>REMOTE_ADDR</th>
              <td>
              <?php
                echo $_SERVER['REMOTE_ADDR'];
              ?></td>
            </tr>
            <tr>
              <th>SERVER_SOFTWARE</th>
              <td>
              <?php
                echo $_SERVER['SERVER_SOFTWARE'];
              ?></td>
            </tr>
            <tr>
              <th>REQUEST_SCHEME</th>
              <td>
              <?php
                echo $_SERVER['REQUEST_SCHEME'];
              ?></td>
            </tr>
            </tbody>
          </table> 
        <?php
          }
        ?>
      </div>
    </div>
<div>

</body>





</html>