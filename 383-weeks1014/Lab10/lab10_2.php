<?php
session_start();
$cnt="";

/* Verify there is a session variable for the counter
   if it doesn't exist, initialize it to zero
   else increase its value
   */
    if( isset($_SESSION['cnt']) ){
        $_SESSION['cnt']++;
        $cnt = $_SESSION['cnt'];
        echo $cnt;
    }else{
        $cnt = 0;
        $_SESSION['cnt']=0;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head></head>
<body>
    <?php if (($cnt%2) == 0):?>
        This is an even number.
    <?php else:?>
        This is an odd number.
    <?php endif;?>
</body>
</html>