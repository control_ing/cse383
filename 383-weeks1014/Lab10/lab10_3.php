<?php
session_start();
$cnt=0;

/*
 Verify there is a session variable for the counter
 if it doesn't exist, initialize it to zero
 Verify there exist an 'option' variable in the URL
 Depending on its value, increase or decrease the counter
 */
if( isset($_SESSION['cnt']) ){
    if( isset($_REQUEST['option']) ){
        $_SESSION['cnt'] += $_REQUEST['option'];
    }
    $cnt = $_SESSION['cnt'];
}else{
    $cnt = 0;
    $_SESSION['cnt']=0;
}

?>

<!DOCTYPE html>
<html lang="en">
<head></head>
<body>
 <form method="GET" action="<?php print $_SERVER['PHP_SELF']; ?>">
 <input type="hidden" name="option" value="-1">
 <button type="submit">-</button>
 </form>
 <?php print $cnt; ?>
 <form method="GET" action="<?php print $_SERVER['PHP_SELF']; ?>">
 <input type="hidden" name="option" value="1">
 <button>+</button>
 </form>
</body>
</html>