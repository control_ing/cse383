<?php
session_start();
$hash="";

/* Update the hash variable, if the corresponding session variable exists
if the user submited a password, after validating the input (special characters), 
create a hash, and save it in the corresponding session variable */
if( isset($_SESSION['pass']) ){
    if(isset($_REQUEST['pass'])){    
        $_SESSION['pass'] = password_hash($_REQUEST['pass'], PASSWORD_DEFAULT);
    }
    $hash = $_SESSION['pass'];
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
 <form method='post' action="<?php print $_SERVER['PHP_SELF'];?>">
 User: <input type="text" name="pass">
 <input type="submit" value="submit">
 </form>
 <?php if ($hash!==""):?>
 <h1>Hash: <?php print $hash;?></h1>
 <?php endif;?>
</body>
</html>