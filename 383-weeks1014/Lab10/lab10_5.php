<?php
session_start();
$pwd = '$2y$10$N0ctRWhrZY5122uUmCXNd.mlyOdX5CgEY3sAH8VbMBGkI/xj2540e';
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta name="generator" content=
"HTML Tidy for HTML5 for Linux version 5.4.0">
<title></title>
</head>
<body>
<?php
//process submit

/* Print the message
 'You were successfully validated'
 when the user provide the correct password, otherwise, print
 'Bad Password -try again'
 */
if( isset($_REQUEST['pwd']) ){
    if(password_verify($_REQUEST['pwd'], $pwd)){
        echo "<h1>You were successfully validated</h1>";
        echo "hash = ",password_hash($_REQUEST['pwd'], PASSWORD_DEFAULT);
    }else{
        echo "<h1>Bad Password -try again</h1>";
        echo "hash = ",password_hash($_REQUEST['pwd'], PASSWORD_DEFAULT);
    }
}
?>

<form method='post' action="
<?php print $_SERVER['PHP_SELF'];?>
">User: 
<input type="password" name="pwd"> <input type="submit" value="submit"></form>
</body>
</html>