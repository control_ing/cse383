<?php

header("Content-Type: Application/json");
require("dataLayer.php");

function getJson() {
    $jsonStringIn = file_get_contents('php://input');
    $json = array();
    $response = array();
    try {
        $json = json_decode($jsonStringIn,true);
        return $json;
    }
    catch (Exception $e) {
        header("HTTP/1.0 500 Invalid content -> probably invalid JSON format");
        $response['status'] = "fail";
        $response['message'] = $e->getMessage();
        print json_encode($response);
        exit;
    }
}

$method = $_SERVER['REQUEST_METHOD'];

if ($method == 'GET') {
    if (isset($_SERVER['PATH_INFO'])){
        $KeyObj = new keyNames();
        if( $_SERVER['PATH_INFO'] == "/v1/keys"){
            print json_encode($KeyObj->getKeyNames());
        }else{
            $requests = explode("/", $_SERVER['PATH_INFO']);            
            if($requests[1]=="v1"&&$requests[2]=="keys"){
                print json_encode($KeyObj->getKeyName($requests[3]));
            }
        }
    }
} elseif ($method=="POST") {
    $body = getJson();
    //$response=array();
    //$response['status'] = "OK";
    //print_r($body);
    $sts = new Tokens();
    //$response["data"] = 
    print json_encode($sts->authenticate($body['username'], $body['password']));
    // if(isset($_SESSION['username']) && isset($_SESSION['password']) ){
    //     $sts = new Tokens();
    //     print json_encode($sts->authenticate($_SESSION['username'], $_SESSION['password']));
    // }

} else {
    header("http/1.1 405 invalid method");
}
?>