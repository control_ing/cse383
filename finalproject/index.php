<?php
   session_start(); 
   $cmd = "";
   if (isset($_GET['cmd'])) {
      $cmd =htmlspecialchars($_GET['cmd']);
      if ($cmd != "author" && $cmd != "page2") {
         $cmd = "";
      }
   }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content=
  "width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Final Project</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  
</head>
<body>

  <div class="container">

<?php
    if ($cmd=="author"){
?>

    <header>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <span class="navbar-brand">Authors</span>
    </nav>

    </header>

    <div class="row">
        <div class="offset-1 col-3">
            <img src="images/author.jpeg">
        </div>
        <div class="offset-1 col-3" style="padding:25px;">
            <p class="text-left">My name is Jianxiang Cui (cuij4). I'm in Computer Science major
            in Miami University. This is my senior year and I'm about to graduate.
            </p>
        </div>
    </div> 
    <div class="row">
        <div class="offset-1 col-3">
            <img src="images/author.jpeg">
        </div>
        <div class="offset-1 col-3" style="padding:25px;">
            <p class="text-left">My name is Bo Wang (wangb12). I'm in Computer Science major
            in Miami University. This is my senior year and I'm about to graduate.
            </p>
        </div>
    </div> 

    <footer>
    <a class="nav-link active" href="index.php?">Return to Login</a>
    </footer>       

<?php
   } else if ($cmd == "app") {
?> 
      HTML for page2 
<?php
   } else {
?>

    <header>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <span class="navbar-brand">Final Project</span>
        </nav>

    </header>

    <div id="main">
    <div class="row">
        <div class="offset-3 col-6">
        <form method='post' id="form" action="<?php print $_SERVER['PHP_SELF'];?>">
            <div class="form-group">
                <label for="user">Username</label>
                <input type="text" class="form-control" id="user" name="user" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
            <button type="submit" class="btn btn-primary">Login</button>
            <div class="form-error alert alert-danger" id="error">
                Username or password is not correct!
            </div>
        </form>
        </div>
    </div>
    
    <div class="row">
        <div class="offset-10">
            <a class="nav-link active" href="index.php?cmd=author">Author</a>
        </div>
    </div>
    </div>

    <div id="app">
        <div class="offset-5"><h1>Diary App</h1></div>
        <div id="buttons" class="row">
            
        </div>

        <div class="row" style="margin:20px;">
        <div class="offset-3 col-6" id="table1">
        
        </div>
        </div>

        <div class="row" style="margin:20px;">
        <div class="offset-2 col-8" id= "table2">
           
        </div>
        </div>
    </div>

<?php
   }
?>

      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <script type="text/javascript" src="js/javascript.js"></script>

</body>
</html>