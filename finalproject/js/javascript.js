$(document).ready(function () {
	var theToken;
	$("#error").hide();
	$('#app').hide();	
	$("form").submit(function (evt) {
			evt.preventDefault();
				var formData = {};
				formData.username = $("#user").val();
				formData.password = $("#password").val();   
				postData(JSON.stringify(formData));			
		});	
	});
	
	function postData(formData){
		$.ajax({
			url: "https://ceclnx01.cec.miamioh.edu/~wangb12/cse383/finalproject/restFinal.php/v1/user",
			type: "POST", 
			data: formData,
	
			success: function( data ) {
					if(data.status=="OK"){
						console.log(data);
						theToken = data.data.token;
						$("#error").hide();	
						$('#main').hide();
						$('#app').show();
						getData();			
					}else {
						$("#error").show();
					}
			},	
			error: function( xhr ) {
				  alert( "The server has thrown an error. Please check console log for details!" ); 
				  console.log( "Error: " + xhr.statusText ); 
				  console.log( "Status: " + xhr.status ); 
			}
		});
	};

	function addData(whichButton){
		var ItemPK = $(whichButton).attr('pk');
		$.ajax({
			url: "https://ceclnx01.cec.miamioh.edu/~wangb12/cse383/finalproject/restFinal.php/v1/items",
			type: "POST", 
			data: {token:theToken, itemFK: ItemPK},
			success: function(data){
				if(data.status=="OK"){
					console.log(data);
				}
			},
			error: function( xhr ) {
				alert( "The server has thrown an error. Please check console log for details!" ); 
				console.log( "Error: " + xhr.statusText ); 
				console.log( "Status: " + xhr.status ); 
		  }
		});
	}

	function getData(){
		$.ajax({
				url: "https://ceclnx01.cec.miamioh.edu/~wangb12/cse383/finalproject/restFinal.php/v1/items",
				type: "GET", 
				success: function( data ) {
					console.log(data);
					if(data.status=="OK"){					
						$.each(data.items, function(index,value){						
							var d = '<div class="offset-1 col-1">';
							d+='<button type="button" '
							d+='pk='+value.pk;
							d+=' onclick="addData(this)"';
							d+=' class="btn btn-primary">';
							d+= value.item;
							d+='</button></div>	';
							$('#buttons').append(d);
						});




						$.ajax({		
							url: "https://ceclnx01.cec.miamioh.edu/~wangb12/cse383/finalproject/restFinal.php/v1/items/" + theToken,
							type: "GET", 
							success: function( data ) {
								if(data.status=='OK'){
									var d = '<table class="table"><thead><tr><th scope="col">Item</th><th scope="col">Timestamp</th></tr></thead><tbody>';
									$.each(data.items, function(index,value){
										d+='<tr>';
										d+= '<td>'+value.item+'</td>';
										d+= '<td>'+value.timestamp+'</td>';									
										d+='</tr>'
									});
									d+='</tbody></table>';
									$('#table2').append(d);




									$.ajax({		
										url: "https://ceclnx01.cec.miamioh.edu/~wangb12/cse383/finalproject/restFinal.php/v1/itemsSummary/" + theToken,
										type: "GET", 
										success: function( data ) {
											if(data.status=='OK'){
												var d = '<table class="table"><thead><tr><th scope="col">Item</th><th scope="col">Count</th></tr></thead><tbody>';
												$.each(data.items, function(index,value){
													d+='<tr>';
													d+= '<td>'+value.item+'</td>';
													d+= '<td>'+value.count+'</td>';									
													d+='</tr>'
												});
												d+='</tbody></table>';
												$('#table1').append(d);
											}
										},	
										error: function( xhr ) {
											alert( "The server has thrown an error. Please check console log for details!" ); 
											console.log( "Error: " + xhr.statusText ); 
											console.log( "Status: " + xhr.status ); 
										}
									});

								}
							},	
							error: function( xhr ) {
								alert( "The server has thrown an error. Please check console log for details!" ); 
								console.log( "Error: " + xhr.statusText ); 
								console.log( "Status: " + xhr.status ); 
							}
						});
					}
			},	
			error: function( xhr ) {
				  alert( "The server has thrown an error. Please check console log for details!" ); 
				  console.log( "Error: " + xhr.statusText ); 
				  console.log( "Status: " + xhr.status ); 
			}
			
		});


		};
