<?php
    require_once("dataLayer.php");
    header("Content-Type: Application/json");
    function getJson() {
        $jsonStringIn = file_get_contents('php://input');
        $json = array();
        $response = array();
        try {
            $json = json_decode($jsonStringIn,true);
            return $json;
        }
        catch (Exception $e) {
        }
    }
    $method = $_SERVER['REQUEST_METHOD'];
    if ($method == 'GET') {
        if (isset($_SERVER['PATH_INFO'])) {
            // $url[0] will be "" empty str
            $url = explode("/", $_SERVER['PATH_INFO']);
            //$KeyValueObj = new KeyNames();
            $ItemsObj = new Items();    
                if ($_SERVER['PATH_INFO'] == "/v1/items") {
                    print(json_encode($ItemsObj->getItems()));
                } else {
                    if ($url[1] == "v1" && $url[2] == "items") {
                        $token = $url[3];
                        print(json_encode($ItemsObj->getItem($token)));
                    }else if($url[1] == "v1" && $url[2] == "itemsSummary"){
                        $token = $url[3];
                        print(json_encode($ItemsObj->getItemsSummary($token)));
                    }
                }
        }
    } else if ($method=="POST") {
        if (isset($_SERVER['PATH_INFO'])) {
            if ($_SERVER['PATH_INFO'] == "/v1/user") {
                $body = getJson();
        //        $response=array();
        //        $response['status'] = "OK";
        //        $response["data"] = $body["data"];
                $sts = new Tokens();
                print json_encode($sts->authenticate($body['username'], $body['password']));
            }
            else if($_SERVER['PATH_INFO'] == "/v1/items"){
                $body = getJson();
                $sts = new Items();
                print json_encode($sts->insertItem($body['token'], $body['itemFK']));
            }
        }
    } else {
        header("http/1.1 405 invalid method");
    }
?>
